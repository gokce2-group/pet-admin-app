package com.gokce.hayvantakip.driver.api.domain;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Customer implements Serializable {
    private String  name;
    private boolean enabled;
    private String  username;
    private long    id;
    private long    creationTime;
}
