package com.gokce.hayvantakip.driver.api.util.exception;

public class AccountExpiredException extends Exception {
    public AccountExpiredException() {
        super("Account expired");
    }
}
