package com.gokce.hayvantakip.driver.api.domain;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Admin implements Serializable {
    private String  name;
    private String  password;
    private boolean enabled;
    private String  username;
    private long    id;
    private String  accessToken;
}
