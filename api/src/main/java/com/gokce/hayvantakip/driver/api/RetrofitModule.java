package com.gokce.hayvantakip.driver.api;

import androidx.annotation.NonNull;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public
class RetrofitModule {
    @Provides
    @Singleton
    Retrofit retrofit(@NonNull OkHttpClient client) {
        return new Retrofit.Builder()
                .client(client)
                .baseUrl("https://hayvantakip.com:8443/takip/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }


    @Provides
    @Singleton
    Interceptor interceptor(@NonNull final TokenHelper tokenHelper) {
        return chain -> {
            if (tokenHelper.getToken() != null) {
                Request request = chain.request().newBuilder()
                        .addHeader("Authorization", tokenHelper.getToken())
                        .addHeader("Accept-Language",
                                Locale.getDefault().getLanguage())
                        .build();
                return chain.proceed(request);
            } else {
                System.out.println("auth token is null so not sending.");
                Request request = chain.request().newBuilder()
                        .addHeader("Accept-Language",
                                Locale.getDefault().getLanguage())
                        .build();
                return chain.proceed(request);

            }
        };
    }

    @Provides
    @Singleton
    OkHttpClient okHttpClient(@NonNull Interceptor interceptor) {
        return new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .callTimeout(1, TimeUnit.MINUTES)
                .build();
    }
}
