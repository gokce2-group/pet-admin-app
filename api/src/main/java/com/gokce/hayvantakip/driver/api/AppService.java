package com.gokce.hayvantakip.driver.api;

import com.gokce.hayvantakip.driver.api.domain.Customer;
import com.gokce.hayvantakip.driver.api.domain.Device;
import com.gokce.hayvantakip.driver.api.domain.util.CustomerRequest;
import com.gokce.hayvantakip.driver.api.domain.util.SaveDeviceRequest;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface AppService {
    @GET("customers/get")
    Single<List<Customer>> getCustomers();

    @GET("devices/get")
    Single<List<Device>> getDevices();

    @POST("devices/save")
    Completable saveDevice(@Body SaveDeviceRequest request);

    @POST("customers/save")
    Completable saveCustomer(@Body CustomerRequest request);
}
