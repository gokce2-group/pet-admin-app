package com.gokce.hayvantakip.driver.api;

import androidx.annotation.NonNull;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class ApiModule {

    @Provides
    @Singleton
    static AppService appService(@NonNull Retrofit retrofit) {
        return retrofit.create(AppService.class);
    }

    @Provides
    @Singleton
    static UserService userService(@NonNull Retrofit retrofit) {
        return retrofit.create(UserService.class);
    }

}
