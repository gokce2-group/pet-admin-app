package com.gokce.hayvantakip.driver.api.util.exception;

public class InvalidPasswordException extends Exception {
    public InvalidPasswordException() {
        super("wrong password");
    }
}
