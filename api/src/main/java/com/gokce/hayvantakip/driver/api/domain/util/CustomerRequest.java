package com.gokce.hayvantakip.driver.api.domain.util;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class CustomerRequest implements Serializable {
    private String  name;
    private long    id;
    private boolean enabled;
    private String  password;
    private String  username;
}
