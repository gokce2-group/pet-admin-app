package com.gokce.hayvantakip.driver.api;

import com.gokce.hayvantakip.driver.api.domain.Admin;
import com.gokce.hayvantakip.driver.api.domain.util.LoginRequest;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface UserService {
    @POST("admins/login")
    Single<Admin> login(@Body LoginRequest request);

}
