package com.gokce.hayvantakip.driver.api.domain.util;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class LoginRequest implements Serializable {
    private final String username;
    private final String password;
    private final String deviceId;
}
