package com.gokce.hayvantakip.driver.api.domain.util;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class SaveDeviceRequest implements Serializable {
    private long   user;
    private String name;
    private long   device;
}
