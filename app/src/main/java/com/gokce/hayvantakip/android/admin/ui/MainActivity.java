package com.gokce.hayvantakip.android.admin.ui;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.gokce.hayvantakip.android.admin.R;
import com.gokce.hayvantakip.android.admin.base.BaseActivity;
import com.gokce.hayvantakip.android.admin.databinding.ActivityMainBinding;
import com.gokce.hayvantakip.android.admin.databinding.NavigationHeaderBinding;
import com.gokce.hayvantakip.android.admin.ui.customer.CustomersFragment;
import com.gokce.hayvantakip.android.admin.ui.device.DevicesFragment;
import com.gokce.hayvantakip.android.admin.util.LogoutPerformer;
import com.gokce.hayvantakip.driver.api.TokenHelper;
import com.gokce.hayvantakip.driver.api.domain.Admin;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import javax.inject.Inject;

import java8.util.Optional;

public class MainActivity extends BaseActivity {
    @Inject
    TokenHelper     tokenHelper;
    @Inject
    LogoutPerformer logoutPerformer;
    private ActivityMainBinding binding;

    private Fragment lastFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Optional<Admin> driverOptional = prefs.getUser();
        if (driverOptional.isEmpty()) {
            startActivityFinishingAffinity(StartActivity.class);
            return;
        }
        tokenHelper.afterLogin(driverOptional.get());
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        setSupportActionBar(binding.appBar);
        setupDrawer();
        setupNavHeader(driverOptional.get());
        String tag = null;
        if (savedInstanceState != null) {
            tag = savedInstanceState.getString("tag");
            if (tag != null) {
                lastFragment = getSupportFragmentManager().findFragmentByTag(tag);
            }
        }
        if (tag == null) {
            tag = "home";
        }
        showFragment(tag);
    }


    private void setupDrawer() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, binding.drawerLayout, binding.appBar, R.string.app_name, R.string.app_name);
        binding.drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        binding.navigationView.setNavigationItemSelectedListener(this::onItemSelected);
    }

    private boolean onItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.itemDevices) {
            showFragment("devices");
        } else if (item.getItemId() == R.id.itemLogout) {
            showLogout();
        } else if (item.getItemId() == R.id.itemCustomers) {
            showFragment("home");
        }
        binding.drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    private void showLogout() {
        new MaterialAlertDialogBuilder(this)
                .setTitle(R.string.logout)
                .setMessage(R.string.message_logout)
                .setPositiveButton(R.string.yes, (dialog, which) -> {
                    logoutPerformer.performAndMoveStart(this);
                })
                .setNegativeButton(R.string.no, null)
                .show();
    }

    private void setupNavHeader(@NonNull Admin admin) {
        NavigationHeaderBinding headerBinding = NavigationHeaderBinding.bind(binding.navigationView.getHeaderView(0));
        headerBinding.nameTextView.setText(admin.getName());
        headerBinding.usernameTextView.setText(admin.getUsername());
    }

    private Fragment createFragment(@NonNull String forTag) {
        switch (forTag) {
            case "home":
                return new CustomersFragment();
            case "devices":
                return new DevicesFragment();
            default:
                return new Fragment();
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        if (lastFragment != null) {
            String tag = lastFragment.getTag();
            outState.putString("tag", tag);
        }
        super.onSaveInstanceState(outState);
    }

    private void showFragment(@NonNull String tag) {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);
        if (fragment == null) {
            fragment = createFragment(tag);
        }
        showFragment(fragment, tag);
    }

    private void showFragment(@NonNull Fragment fragment,
                              @NonNull String tag) {
        FragmentTransaction transaction = getSupportFragmentManager()
                .beginTransaction();
        if (getSupportFragmentManager().findFragmentByTag(tag) == null) {
            transaction.add(R.id.fragmentFrame, fragment, tag);
        }
        if (lastFragment != null && !tag.equals(lastFragment.getTag())) {
            transaction.hide(lastFragment);
        }
        transaction.show(fragment);
        transaction.commit();
        lastFragment = fragment;
        afterFragmentChanged();
    }

    private void afterFragmentChanged() {
        if (lastFragment instanceof CustomersFragment) {
            setTitle(R.string.customers);
        } else if (lastFragment instanceof DevicesFragment) {
            setTitle(R.string.devices);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_activity_menu, menu);
        menu.findItem(R.id.itemAdd).getIcon()
                .setTint(ContextCompat.getColor(this, R.color.primaryColor));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.itemAdd) {
            if (lastFragment instanceof DevicesFragment) {
                DetailActivity.showSaveDevice(this, null);
            } else if (lastFragment instanceof CustomersFragment) {
                DetailActivity.showSaveCustomer(this, null);
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
