package com.gokce.hayvantakip.android.admin.util;

import android.app.Activity;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.gokce.hayvantakip.android.admin.ui.StartActivity;
import com.gokce.hayvantakip.driver.api.TokenHelper;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;
import javax.inject.Singleton;

import java8.util.function.Consumer;

@Singleton
public class LogoutPerformer {
    private final Prefs       prefs;
    private final TokenHelper tokenHelper;
    @Inject
    LogoutPerformer(@NonNull Prefs prefs,
                    @NonNull TokenHelper tokenHelper) {
        this.prefs       = prefs;
        this.tokenHelper = tokenHelper;
    }

    public void perform(@Nullable Consumer<Void> onSuccess) {
        tokenHelper.voidAuth();
        prefs.clear();
        EventBus.getDefault().post(new LogoutEvent());
        if (onSuccess != null) {
            onSuccess.accept(null);
        }
    }

    public void performAndMoveStart(@NonNull Activity context) {
        perform(aVoid -> {
            context.startActivity(new Intent(context, StartActivity.class));
            context.finishAffinity();
        });
    }
}
