package com.gokce.hayvantakip.android.admin.ui.device;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.gokce.hayvantakip.android.admin.R;
import com.gokce.hayvantakip.android.admin.base.DataFragment;
import com.gokce.hayvantakip.android.admin.ui.DetailActivity;
import com.gokce.hayvantakip.driver.api.AppService;
import com.gokce.hayvantakip.driver.api.domain.Device;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

public class DevicesFragment extends DataFragment<Device> {
    @Inject
    AppService appService;

    @NonNull
    @Override
    public RecyclerView.Adapter getAdapter(@NonNull List<Device> data) {
        return new DevicesAdapter(data, this::onEdit);
    }

    private void onEdit(@NonNull Device device) {
        DetailActivity.showSaveDevice(context, device);
    }

    @NonNull
    @Override
    public Observable<List<Device>> getCall() {
        return appService.getDevices().toObservable();
    }

    @Override
    public int getErrorIcon() {
        return R.drawable.ic_developer_board_black_24dp;
    }

    @Override
    public void onResume() {
        super.onResume();
        refresh();
    }
}
