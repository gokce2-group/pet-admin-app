package com.gokce.hayvantakip.android.admin.ui.customer;

import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.gokce.hayvantakip.android.admin.base.BaseRecyclerAdapter;
import com.gokce.hayvantakip.driver.api.domain.Customer;

import java.util.List;

public class CustomerAdapter extends BaseRecyclerAdapter<Customer, CustomerViewHolder> {
    private final CustomerViewHolder.OnEditClickListener editClickListener;

    CustomerAdapter(@NonNull List<Customer> items,
                    CustomerViewHolder.OnEditClickListener editClickListener) {
        super(items);
        this.editClickListener = editClickListener;
    }

    @NonNull
    @Override
    public CustomerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return CustomerViewHolder.create(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomerViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        holder.setEditClickListener(editClickListener);
    }
}
