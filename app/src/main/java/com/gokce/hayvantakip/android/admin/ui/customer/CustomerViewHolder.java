package com.gokce.hayvantakip.android.admin.ui.customer;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.gokce.hayvantakip.android.admin.R;
import com.gokce.hayvantakip.android.admin.base.BaseViewHolder;
import com.gokce.hayvantakip.android.admin.databinding.ItemCustomerBinding;
import com.gokce.hayvantakip.android.admin.helper.Helper;
import com.gokce.hayvantakip.driver.api.domain.Customer;

public class CustomerViewHolder extends BaseViewHolder<Customer> {
    private ItemCustomerBinding binding;
    private OnEditClickListener editClickListener;

    private CustomerViewHolder(@NonNull View itemView) {
        super(itemView);
        binding = ItemCustomerBinding.bind(itemView);
    }

    @NonNull
    static CustomerViewHolder create(@NonNull ViewGroup parent) {
        return new CustomerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_customer, parent, false));
    }

    public void setEditClickListener(OnEditClickListener editClickListener) {
        this.editClickListener = editClickListener;
    }

    @Override
    public <X extends Customer> void bind(@NonNull X t) {
        Glide.with(binding.imageView)
                .load(Helper.identityIcon(t.getUsername() + "@hayantakip.com"))
                .placeholder(R.drawable.ic_account_circle_black_24dp)
                .into(binding.imageView);
        binding.nameTextView.setText(t.getName());
        binding.usernameTextView.setText(t.getUsername());
        binding.timeTextView.setText(Helper.getFormattedTime(t.getCreationTime()));
        binding.editButton.setOnClickListener(v -> {
            if (editClickListener != null) {
                editClickListener.onEdit(t);
            }
        });
    }

    public interface OnEditClickListener {
        void onEdit(@NonNull Customer customer);
    }
}
