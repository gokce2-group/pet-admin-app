package com.gokce.hayvantakip.android.admin.injection;

import com.gokce.hayvantakip.android.admin.App;
import com.gokce.hayvantakip.driver.api.ApiModule;
import com.gokce.hayvantakip.driver.api.RetrofitModule;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.AndroidInjectionModule;
import dagger.android.AndroidInjector;

@Component(modules = {AppContextModule.class, AndroidInjectionModule.class,
        ActivityModule.class, FragmentModule.class, RetrofitModule.class,
        ApiModule.class})
@Singleton
public interface AppComponent extends AndroidInjector<App> {
}
