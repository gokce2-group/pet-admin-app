package com.gokce.hayvantakip.android.admin.util;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;

import com.gokce.hayvantakip.driver.api.domain.Admin;
import com.google.gson.Gson;

import javax.inject.Inject;
import javax.inject.Singleton;

import java8.util.Optional;

@Singleton
public class Prefs {
    private final SharedPreferences sharedPreferences;
    private final Gson              transformer;

    @Inject
    Prefs(@NonNull Context context) {
        sharedPreferences = context.getSharedPreferences("water.driver.prefs", Context.MODE_PRIVATE);
        transformer       = new Gson();
    }

    public void saveUser(@NonNull Admin user) {
        String json = transformer.toJson(user);
        sharedPreferences.edit().putString("driver.user", json).apply();
    }

    @NonNull
    public Optional<Admin> getUser() {
        String json = sharedPreferences.getString("driver.user", null);
        if (json == null) {
            return Optional.empty();
        }
        try {
            return Optional.ofNullable(transformer.fromJson(json, Admin.class));
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    public void clear() {
        sharedPreferences.edit().clear().apply();
    }
}
