package com.gokce.hayvantakip.android.admin.ui.customer;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.gokce.hayvantakip.android.admin.R;
import com.gokce.hayvantakip.android.admin.base.BaseFragment;
import com.gokce.hayvantakip.android.admin.databinding.FragmentSaveCustomerBinding;
import com.gokce.hayvantakip.android.admin.util.DialogUtil;
import com.gokce.hayvantakip.driver.api.AppService;
import com.gokce.hayvantakip.driver.api.domain.Customer;
import com.gokce.hayvantakip.driver.api.domain.util.CustomerRequest;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class SaveCustomerFragment extends BaseFragment {
    @Inject
    AppService appService;
    private FragmentSaveCustomerBinding binding;
    private Customer                    customer;
    private Disposable                  disposable;

    @NonNull
    public static SaveCustomerFragment newInstance(@Nullable Customer c) {
        Bundle args = new Bundle();
        args.putSerializable("customer", c);
        SaveCustomerFragment fragment = new SaveCustomerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            customer = (Customer) getArguments().getSerializable("customer");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentSaveCustomerBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (customer != null) {
            applyValues(customer);
        }
        binding.buttonSave.setOnClickListener(v -> onSaveTap());
    }

    private void applyValues(@NonNull Customer customer) {
        binding.userEditText.setEnabled(false);
        binding.userEditText.setText(customer.getUsername());
        binding.nameEditText.setText(customer.getName());
        binding.passwordEditLayout.setHint(getString(R.string.change_password));
        binding.enableSwitch.setChecked(customer.isEnabled());
    }

    private void onSaveTap() {
        CharSequence name            = binding.nameEditText.getText();
        CharSequence password        = binding.passwordEditText.getText();
        CharSequence confirmPassword = binding.confirmPasswordEditText.getText();
        CharSequence username        = binding.userEditText.getText();
        if (name == null || username == null) {
            showError(getString(R.string.please_input_all_values));
            return;
        }
        String strName     = name.toString().trim();
        String strUsername = username.toString().trim();
        if (TextUtils.isEmpty(strName) || (customer == null && TextUtils.isEmpty(strUsername))) {
            showError(getString(R.string.please_input_all_values));
            return;
        }
        String  strPassword       = null;
        boolean matched           = confirmPassword != null && password != null && password.toString().equals(confirmPassword.toString().trim());
        boolean passwordCondition = password != null && password.toString().trim().length() > 2;
        if (customer == null) {
            if (!passwordCondition) {
                showError(getString(R.string.please_input_valid));
                return;
            }
            if (!matched) {
                showError(getString(R.string.password_not_match));
                return;
            }
            strPassword = password.toString().trim();
        } else if (!TextUtils.isEmpty(password)) {
            if (!passwordCondition) {
                showError(getString(R.string.please_input_valid));
                return;
            }
            if (!matched) {
                showError(getString(R.string.password_not_match));
                return;
            }
            strPassword = password.toString().trim();
        }


        toggleProgress(true);
        disposable = appService.saveCustomer(CustomerRequest.builder()
                .enabled(binding.enableSwitch.isChecked())
                .id(customer != null ? customer.getId() : 0)
                .password(strPassword)
                .name(strName)
                .username(strUsername)
                .build())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onDone, error -> {
                    toggleProgress(false);
                    showError(error.getLocalizedMessage());
                });


    }

    private void onDone() {
        toggleProgress(false);
        activity().ifPresent(a -> {
            new MaterialAlertDialogBuilder(a)
                    .setMessage(R.string.customer_save_success)
                    .setPositiveButton(R.string.ok, null)
                    .setOnDismissListener(dialog -> a.finish())
                    .show();
        });
    }

    private void toggleProgress(boolean show) {
        binding.buttonSave.toggleProgress(show);
        binding.userEditText.setEnabled(!show);
        binding.nameEditText.setEnabled(!show);
        binding.enableSwitch.setEnabled(!show);
        binding.confirmPasswordEditText.setEnabled(!show);
        binding.passwordEditText.setEnabled(!show);
    }

    private void showError(String message) {
        activity().ifPresent(a -> {
            DialogUtil.showOk(a, message);
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (disposable != null) {
            disposable.dispose();
        }
    }
}
