package com.gokce.hayvantakip.android.admin.ui.device;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.gokce.hayvantakip.android.admin.R;
import com.gokce.hayvantakip.android.admin.base.BaseViewHolder;
import com.gokce.hayvantakip.android.admin.databinding.ItemDeviceBinding;
import com.gokce.hayvantakip.driver.api.domain.Device;

public class DeviceViewHolder extends BaseViewHolder<Device> {
    private ItemDeviceBinding   binding;
    private OnEditClickListener editClickListener;

    private DeviceViewHolder(@NonNull View itemView) {
        super(itemView);
        binding = ItemDeviceBinding.bind(itemView);
    }

    @NonNull
    static DeviceViewHolder create(@NonNull ViewGroup parent) {
        return new DeviceViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_device, parent, false));
    }

    public void setEditClickListener(OnEditClickListener editClickListener) {
        this.editClickListener = editClickListener;
    }

    @Override
    public <X extends Device> void bind(@NonNull X t) {
        if (t.isAssigned()) {
            binding.assignTextView.setText(getString(R.string.assigned_to, t.getUser().getName()));
        } else {
            binding.assignTextView.setText(R.string.not_assigned);
        }
        binding.idTextView.setText(getString(R.string.format_id, t.getId()));
        binding.nameTextView.setText(t.getName());
        binding.editButton.setOnClickListener(v -> {
            if (editClickListener != null) {
                editClickListener.onEdit(t);
            }
        });
    }

    public interface OnEditClickListener {
        void onEdit(@NonNull Device device);
    }
}
