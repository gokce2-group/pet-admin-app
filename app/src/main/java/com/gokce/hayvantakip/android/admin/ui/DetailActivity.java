package com.gokce.hayvantakip.android.admin.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.gokce.hayvantakip.android.admin.R;
import com.gokce.hayvantakip.android.admin.base.BaseToolbarActivity;
import com.gokce.hayvantakip.android.admin.ui.customer.SaveCustomerFragment;
import com.gokce.hayvantakip.android.admin.ui.device.SaveDeviceFragment;
import com.gokce.hayvantakip.driver.api.domain.Customer;
import com.gokce.hayvantakip.driver.api.domain.Device;

public class DetailActivity extends BaseToolbarActivity {


    public static void showSaveDevice(@NonNull Context context,
                                      @Nullable Device device) {
        context.startActivity(new Intent(context, DetailActivity.class)
                .putExtra("device", true)
                .putExtra("data", device));
    }

    public static void showSaveCustomer(@NonNull Context context,
                                        @Nullable Customer customer) {
        context.startActivity(new Intent(context, DetailActivity.class)
                .putExtra("customer", true)
                .putExtra("data", customer));
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        if (getIntent().getBooleanExtra("device", false)) {
            Device data = (Device) getIntent().getSerializableExtra("data");
            if (data != null) {
                setTitle(getString(R.string.format_id, data.getId()));
            } else {
                setTitle(R.string.add_device);
            }
            showFragment(SaveDeviceFragment.newInstance(data));
            return;
        } else if (getIntent().getBooleanExtra("customer", false)) {
            Customer customer = (Customer) getIntent().getSerializableExtra("data");
            if (customer != null) {
                setTitle(customer.getName());
            } else {
                setTitle(R.string.add_customer);
            }
            showFragment(SaveCustomerFragment.newInstance(customer));
            return;
        }
        finish();
    }

    private void showFragment(@NonNull Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.fragmentFrame, fragment).commit();
    }
}
