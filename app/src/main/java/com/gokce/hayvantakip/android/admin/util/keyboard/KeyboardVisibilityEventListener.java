package com.gokce.hayvantakip.android.admin.util.keyboard;

public interface KeyboardVisibilityEventListener {

    void onVisibilityChanged(boolean isOpen);
}
