package com.gokce.hayvantakip.android.admin.ui.device;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.gokce.hayvantakip.android.admin.R;
import com.gokce.hayvantakip.android.admin.base.BaseFragment;
import com.gokce.hayvantakip.android.admin.databinding.FragmentSaveDeviceBinding;
import com.gokce.hayvantakip.android.admin.databinding.SelectionChipViewBinding;
import com.gokce.hayvantakip.android.admin.util.DialogUtil;
import com.gokce.hayvantakip.driver.api.AppService;
import com.gokce.hayvantakip.driver.api.domain.Customer;
import com.gokce.hayvantakip.driver.api.domain.Device;
import com.gokce.hayvantakip.driver.api.domain.util.SaveDeviceRequest;
import com.google.android.material.chip.Chip;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java8.util.Objects;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

public class SaveDeviceFragment extends BaseFragment {
    private static final String TAG = SaveDeviceFragment.class.getSimpleName();
    @Inject
    AppService appService;
    private FragmentSaveDeviceBinding binding;
    private CompositeDisposable       disposable;
    private Device                    device;

    private Customer       assignedCustomer;
    private List<Customer> customers;

    private TextWatcher searchTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            performSearch(binding.searchEditText.getText());
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    @NonNull
    public static SaveDeviceFragment newInstance(@Nullable Device device) {
        Bundle args = new Bundle();
        args.putSerializable("device", device);
        SaveDeviceFragment fragment = new SaveDeviceFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            device = (Device) getArguments().getSerializable("device");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentSaveDeviceBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (device != null) {
            applySaved(device);
            assignedCustomer = device.getUser();
        }
        disposable = new CompositeDisposable();
        loadCustomers();
        binding.searchEditText.addTextChangedListener(searchTextWatcher);
        binding.buttonSave.setOnClickListener(v -> onSaveTap());
    }

    private void loadCustomers() {
        Disposable d = appService.getCustomers()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(customers -> {
                    this.customers = customers;
                    setupCustomers(customers);
                }, error -> {
                    Log.e(TAG, "loadCustomers: failed because " + error.getLocalizedMessage());
                });
        disposable.add(d);
    }

    private void setupCustomers(@NonNull List<Customer> customers) {
        binding.customersChipGroup.removeAllViews();
        for (Customer customer : customers) {
            SelectionChipViewBinding binding = SelectionChipViewBinding.inflate(LayoutInflater.from(context));
            Chip                     chip    = (Chip) binding.getRoot();
            chip.setId(View.generateViewId());
            chip.setTag(customer.getId());
            chip.setText(customer.getName());
            if (device != null && device.isAssigned() && device.getUser().getId() == customer.getId()) {
                chip.setChecked(true);
            }
            chip.setOnClickListener(v -> {
                long tag = (long) v.getTag();
                this.binding.customersChipGroup.clearCheck();
                if (assignedCustomer != null && tag == assignedCustomer.getId()) {
                    assignedCustomer = null;
                } else {
                    this.binding.customersChipGroup.check(v.getId());
                    assignedCustomer = customer;
                }
            });
            this.binding.customersChipGroup.addView(chip);
        }
    }

    private void applySaved(@NonNull Device device) {
        binding.nameEditText.setText(device.getName());
        if (device.isAssigned()) {
            binding.assignTextView.setText(getString(R.string.assigned_but_search, device.getUser().getName()));
        }
    }

    private void performSearch(@Nullable CharSequence s) {
        if (customers == null) {
            return;
        }
        if (TextUtils.isEmpty(s)) {
            setupCustomers(customers);
            return;
        }
        String text = s.toString().toLowerCase();
        List<Customer> customers = StreamSupport.stream(this.customers)
                .filter(customer -> Objects.nonNull(customer.getName()) && Objects.nonNull(customer.getUsername()))
                .filter(customer -> {
                    String name     = customer.getName().toLowerCase();
                    String username = customer.getUsername().toLowerCase();
                    return name.contains(text) || username.contains(text);
                })
                .collect(Collectors.toList());
        setupCustomers(customers);
    }

    private void onSaveTap() {
        CharSequence name = binding.nameEditText.getText();
        if (TextUtils.isEmpty(name)) {
            showError(getString(R.string.please_input_name));
            return;
        }
        String strName = name.toString().trim();
        if (TextUtils.isEmpty(strName)) {
            showError(getString(R.string.please_input_name));
            return;
        }
        SaveDeviceRequest request = SaveDeviceRequest.builder()
                .device(device != null ? device.getId() : 0)
                .name(strName)
                .user(assignedCustomer != null ? assignedCustomer.getId() : 0)
                .build();
        toggleProgress(true);
        Disposable d = appService.saveDevice(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onDone, error -> {
                    toggleProgress(false);
                    showError(error.getLocalizedMessage());
                });
        disposable.add(d);
    }

    private void onDone() {
        toggleProgress(false);
        activity().ifPresent(a -> {
            new MaterialAlertDialogBuilder(a)
                    .setMessage(R.string.device_save_success)
                    .setPositiveButton(R.string.ok, null)
                    .setOnDismissListener(dialog -> a.finish())
                    .show();
        });
    }

    private void toggleProgress(boolean show) {
        binding.buttonSave.toggleProgress(show);
        binding.searchEditText.setEnabled(!show);
        binding.nameEditText.setEnabled(!show);
        binding.customersChipGroup.setEnabled(!show);
    }

    private void showError(String message) {
        activity().ifPresent(a -> {
            DialogUtil.showOk(a, message);
        });
    }

    @Override
    public void onDestroyView() {
        if (disposable != null) {
            disposable.dispose();
        }
        super.onDestroyView();
    }
}
