package com.gokce.hayvantakip.android.admin.injection;

import com.gokce.hayvantakip.android.admin.ui.customer.CustomersFragment;
import com.gokce.hayvantakip.android.admin.ui.customer.SaveCustomerFragment;
import com.gokce.hayvantakip.android.admin.ui.device.DevicesFragment;
import com.gokce.hayvantakip.android.admin.ui.device.SaveDeviceFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FragmentModule {
    @ContributesAndroidInjector
    abstract CustomersFragment customersFragment();

    @ContributesAndroidInjector
    abstract DevicesFragment devicesFragment();

    @ContributesAndroidInjector
    abstract SaveDeviceFragment saveDeviceFragment();

    @ContributesAndroidInjector
    abstract SaveCustomerFragment saveCustomerFragment();
}
