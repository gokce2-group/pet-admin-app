package com.gokce.hayvantakip.android.admin.ui.device;

import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.gokce.hayvantakip.android.admin.base.BaseRecyclerAdapter;
import com.gokce.hayvantakip.driver.api.domain.Device;

import java.util.List;

public class DevicesAdapter extends BaseRecyclerAdapter<Device, DeviceViewHolder> {
    private final DeviceViewHolder.OnEditClickListener editClickListener;

    DevicesAdapter(@NonNull List<Device> items,
                   DeviceViewHolder.OnEditClickListener editClickListener) {
        super(items);
        this.editClickListener = editClickListener;
    }

    @NonNull
    @Override
    public DeviceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return DeviceViewHolder.create(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull DeviceViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        holder.setEditClickListener(editClickListener);
    }
}
