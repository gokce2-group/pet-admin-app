package com.gokce.hayvantakip.android.admin.injection;


import com.gokce.hayvantakip.android.admin.ui.DetailActivity;
import com.gokce.hayvantakip.android.admin.ui.MainActivity;
import com.gokce.hayvantakip.android.admin.ui.StartActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityModule {
    @ContributesAndroidInjector
    abstract MainActivity mainActivity();

    @ContributesAndroidInjector
    abstract StartActivity startActivity();

    @ContributesAndroidInjector
    abstract DetailActivity detailActivity();
}
