package com.gokce.hayvantakip.android.admin.ui.customer;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.gokce.hayvantakip.android.admin.R;
import com.gokce.hayvantakip.android.admin.base.DataFragment;
import com.gokce.hayvantakip.android.admin.ui.DetailActivity;
import com.gokce.hayvantakip.driver.api.AppService;
import com.gokce.hayvantakip.driver.api.domain.Customer;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

public class CustomersFragment extends DataFragment<Customer> {
    @Inject
    AppService appService;

    @NonNull
    @Override
    public RecyclerView.Adapter getAdapter(@NonNull List<Customer> data) {
        return new CustomerAdapter(data, this::onEdit);
    }

    private void onEdit(@NonNull Customer customer) {
        DetailActivity.showSaveCustomer(context, customer);
    }

    @NonNull
    @Override
    public Observable<List<Customer>> getCall() {
        return appService.getCustomers().toObservable();
    }

    @Override
    public int getErrorIcon() {
        return R.drawable.ic_account_circle_black_24dp;
    }

    @Override
    public void onResume() {
        super.onResume();
        refresh();
    }
}
