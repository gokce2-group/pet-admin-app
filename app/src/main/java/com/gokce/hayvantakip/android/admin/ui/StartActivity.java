package com.gokce.hayvantakip.android.admin.ui;

import android.os.Bundle;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.gokce.hayvantakip.android.admin.R;
import com.gokce.hayvantakip.android.admin.base.BaseActivity;
import com.gokce.hayvantakip.android.admin.databinding.ActivityStartBinding;
import com.gokce.hayvantakip.android.admin.util.DialogUtil;
import com.gokce.hayvantakip.android.admin.util.Installation;
import com.gokce.hayvantakip.driver.api.TokenHelper;
import com.gokce.hayvantakip.driver.api.UserService;
import com.gokce.hayvantakip.driver.api.domain.util.LoginRequest;
import com.gokce.hayvantakip.driver.api.util.exception.ApiExceptionUtil;
import com.gokce.hayvantakip.driver.api.util.exception.NoInternetException;
import com.gokce.hayvantakip.driver.api.util.exception.NotFoundException;
import com.gokce.hayvantakip.driver.api.util.exception.WrongPasswordException;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class StartActivity extends BaseActivity {
    @Inject
    UserService userService;
    @Inject
    TokenHelper tokenHelper;
    private ActivityStartBinding binding;
    private Disposable           disposable;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityStartBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        binding.btnLogin.setOnClickListener(v -> onLoginClick());
    }

    private void onLoginClick() {
        CharSequence username = binding.etUsername.getText();
        CharSequence password = binding.etPassword.getText();
        if (username == null || password == null) {
            return;
        }
        username = username.toString().trim();
        password = password.toString().trim();
        if (TextUtils.isEmpty(username) || TextUtils.isEmpty(password)) {
            return;
        }
        String       deviceId   = Installation.id(getApplicationContext());
        final String stringPass = password.toString();
        toggleProgress(true);
        disposable = userService.login(new LoginRequest(username.toString(), stringPass, deviceId))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(error -> Single.error(ApiExceptionUtil.loginException(error)))
                .subscribe(driver -> {
                    tokenHelper.afterLogin(driver);
                    driver.setPassword(stringPass);
                    prefs.saveUser(driver);
                    startActivityFinishingAffinity(MainActivity.class);
                }, this::handleLoginError);
    }

    private void handleLoginError(@NonNull Throwable error) {
        toggleProgress(false);
        if (error instanceof WrongPasswordException) {
            DialogUtil.showOk(this, R.string.wrong_password);
        } else if (error instanceof NotFoundException) {
            DialogUtil.showOk(this, R.string.no_user_found);
        } else if (error instanceof NoInternetException) {
            DialogUtil.showOk(this, R.string.no_internet);
        } else {
            DialogUtil.showOk(this, error.getLocalizedMessage());
        }
    }

    @Override
    protected void onStop() {
        if (disposable != null) {
            disposable.dispose();
        }
        super.onStop();
    }

    private void toggleProgress(boolean show) {
        binding.btnLogin.toggleProgress(show);
        binding.etPassword.setEnabled(!show);
        binding.etUsername.setEnabled(!show);
    }
}
